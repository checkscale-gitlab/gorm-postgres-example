module gitlab.com/VagueCoder0to.n/GORM-Postgres-Example/app

go 1.13

require (
	github.com/jinzhu/gorm v1.9.16
	github.com/stretchr/testify v1.7.0
)
