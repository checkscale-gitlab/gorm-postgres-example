package database

import (
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateDatabase(t *testing.T) {
	_, err := NewDBClient(&log.Logger{})
	if err != nil {
		flag := true
		t.Run("TestGORMConnection", func(t *testing.T) {
			flag = flag && assert.NotContains(t, err.Error(), "gorm connection open", "Failed to Open GORM Connection")
		})

		if flag {
			t.Run("TestDBPing", func(t *testing.T) {
				flag = flag && assert.NotContains(t, err.Error(), "db ping", "Failed to Ping DB")
			})
		}
		if flag {
			t.Run("TestUndefinedError", func(t *testing.T) {
				assert.Fail(t, err.Error(), "Undefined error")
			})
		}
	}
}
